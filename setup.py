from setuptools import setup

package_info = {}
with open("version.py") as version_py:
    exec(version_py.read(), package_info)
del package_info["__builtins__"]

with open("README.md") as readme_file:
    readme = readme_file.read()

setup(
    **package_info,
    long_description=readme,
    long_description_content_type="text/markdown",
    py_modules=["timethis.__init__", "timethis.context", "timethis.format"],
)
