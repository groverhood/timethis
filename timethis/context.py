"""
Implementation of :class:`TimingContext <timethis.context.TimingContext>`.
"""

from __future__ import annotations

import typing as _t
import time as _time
import functools as _fn
import contextlib as _c
import collections as _col
import weakref as _w

from types import TracebackType as _TracebackType
from . import format as _f

_R = _t.TypeVar("_R")
_AnyCallable = _t.TypeVar("_AnyCallable", bound=_t.Callable)


class _TimeRecord:
    _timing: bool
    _ptimes: list[float]
    _rtimes: list[float]
    _times: _timesview

    class _timesview:
        def __init__(self, ptimes: list[float], rtimes: list[float]) -> None:
            if len(ptimes) != len(rtimes):
                raise ValueError("Provided uneven ptime and rtime lists")

            self._ptimes = ptimes
            self._rtimes = rtimes

        def __getitem__(self, index: int) -> tuple[float, float]:
            return self._ptimes[index], self._rtimes[index]

        def __len__(self) -> int:
            return len(self._ptimes)

        def __iter__(self) -> _t.Iterator[tuple[float, float]]:
            return zip(self._ptimes, self._rtimes)

    def __init__(self) -> None:
        self._timing = False
        self._ptimes = []
        self._rtimes = []
        self._times = _TimeRecord._timesview(self._ptimes, self._rtimes)

    @property
    def times(self) -> _timesview:
        return self._times

    @property
    def total_runs(self) -> int:
        return len(self._ptimes)

    @property
    def average_ptime(self) -> float:
        return sum(self._ptimes) / self.total_runs

    @property
    def average_rtime(self) -> float:
        return sum(self._rtimes) / self.total_runs

    @property
    def best_ptime(self) -> float:
        return min(self._ptimes)

    @property
    def worst_ptime(self) -> float:
        return max(self._ptimes)

    @property
    def best_rtime(self) -> float:
        return min(self._rtimes)

    @property
    def worst_rtime(self) -> float:
        return max(self._rtimes)


class TimingContext(_c.AbstractContextManager):
    """
    State manager for timing data. This can be used as a context manager
    or a decorator with :meth:`timethis() <TimingContext.timethis>`.
    """

    _format: _f.format
    _file: _t.Optional[_t.IO]
    _fileowner: bool
    _numdigits: int

    _time_database: dict[str, _TimeRecord]
    _finalizer: _w.finalize
    _cleaned_up: bool

    _context_stack: list[TimingContext] = []

    @staticmethod
    def current() -> _t.Optional[TimingContext]:
        """The current TimingContext if being used as a context manager."""
        return (
            TimingContext._context_stack[-1]
            if len(TimingContext._context_stack) > 0
            else None
        )

    @property
    def file(self) -> _t.Optional[_t.IO]:
        """The I/O stream being written to during cleanup."""
        return self._file

    def __init__(
        self,
        *,
        format: _t.Union[str, _f.format] = _f.human,
        file: _t.Union[str, _t.IO] = None,
        numdigits: int = 7
    ) -> None:
        super().__init__()
        self._format = _f.stringformat(format) if isinstance(format, str) else format
        if isinstance(file, str):
            self._fileowner = True
            self._file = open(file, "w+")
        else:
            self._fileowner = False
            self._file = file
        self._numdigits = numdigits
        self._time_database = _col.defaultdict(_TimeRecord)
        self._finalizer = _w.finalize(self, self._cleanup)
        self._cleaned_up = False

    def __enter__(self):
        TimingContext._context_stack.append(self)
        return self

    def timeit(self, func: _t.Callable[..., _R], *args, **kwds) -> _R:
        """
        Time ``func``'s runtime in both real time and CPU time when called
        with ``args`` and ``kwds``.

        :param func: The function that is being timed.
        :type func: Callable[..., _R]

        :param args: Positional arguments.

        :param kwds: Keyword arguments.

        :return: The result of calling the function with the provided
            arguments.
        :rtype: _R
        """
        record = self._time_database[func.__name__]
        if record._timing:
            return func(*args, **kwds)
        record._timing = True

        rtime_start, ptime_start = _time.time(), _time.process_time()
        result = func(*args, **kwds)
        rtime_stop, ptime_stop = _time.time(), _time.process_time()

        record._timing = False

        record._rtimes.append(rtime_stop - rtime_start)
        record._ptimes.append(ptime_stop - ptime_start)

        return result

    def timethis(self, func: _AnyCallable) -> _AnyCallable:
        """
        Wraps ``func`` with a partial application of
        :meth:`timeit <timethis.context.TimingContext>` on the function
        argument.

        :param func: The function to be timed.
        :type func: _AnyCallable

        :return: The wrapped function.
        :rtype: _AnyCallable
        """
        return _t.cast(_AnyCallable, _fn.wraps(func)(_fn.partial(self.timeit, func)))

    def _cleanup(self) -> None:
        if not self._cleaned_up:
            self._cleaned_up = True
            if len(self._format.opener) > 0:
                print(self._format.opener, file=self._file)
            for function, record in self._time_database.items():
                print(
                    self._format.format(
                        function_name=function,
                        avg_ptime=round(record.average_ptime, ndigits=self._numdigits),
                        best_ptime=round(record.best_ptime, ndigits=self._numdigits),
                        worst_ptime=round(record.worst_ptime, ndigits=self._numdigits),
                        avg_rtime=round(record.average_rtime, ndigits=self._numdigits),
                        best_rtime=round(record.best_rtime, ndigits=self._numdigits),
                        worst_rtime=round(record.worst_rtime, ndigits=self._numdigits),
                    ),
                    file=self._file,
                )
            if self._fileowner:
                # mypy isn't smart enough to detect that self._fileowner being
                # set and self._file being None are mutually exclusive
                self._file.close()  # type: ignore

    def __iter__(self) -> _t.Iterator[tuple[str, _TimeRecord]]:
        return iter(self._time_database.items())

    def __getitem__(self, key: _t.Union[_AnyCallable, str]) -> _TimeRecord:
        if isinstance(key, str):
            return self._time_database[key]
        return self._time_database[key.__name__]

    def __exit__(
        self,
        exc_type: _t.Optional[_t.Type[BaseException]],
        exc_value: _t.Optional[BaseException],
        traceback: _t.Optional[_TracebackType],
    ) -> None:
        if exc_type is None:
            self._cleanup()
        TimingContext._context_stack.pop(-1)
