"""
The top-level module in **timethis**. It exposes the 
:func:`timethis <timethis.timethis>` decorator as well as the 
:class:`TimingContext <timethis.context.TimingContext>` class from 
:mod:`timethis.context`.
"""

import functools as _f
import typing as _t

from .context import TimingContext

_AnyCallable = _t.TypeVar("_AnyCallable", bound=_t.Callable)


def timethis(func: _AnyCallable) -> _AnyCallable:
    """
    Decorate a function to label it as a target for time data collection. This
    decorator makes use of TimingContext as a context manager.

    :param func: A function to be timed by the current context.
    :type func: _AnyCallable

    :return: A wrapped version of  ``func`` that calls it through the current
        context's :meth:`timeit <timethis.context.TimingContext.timeit>` method.
    :rtype: _AnyCallable
    """

    @_f.wraps(func)
    def wrapper(*args, **kwds):
        context = TimingContext.current()
        return context.timeit(func, *args, **kwds)

    return _t.cast(_AnyCallable, wrapper)
