import abc as _abc


class format:
    """Formats the input arguments following an optional opener"""

    @_abc.abstractproperty
    def opener(self) -> str:
        """
        A string that is outputted before any formatted table output if not
        empty.
        """
        ...

    @_abc.abstractmethod
    def format(self, *args, **kwds) -> str:
        """
        Create a formatted string based off of the provided arguments.

        :param args: Positional arguments.
        :param kwds: Keyword arguments.
        """
        ...


class stringformat(format):
    """Wrapper for str.format()"""

    _str: str
    _opener: str

    @property
    def opener(self) -> str:
        return self._opener

    def __init__(self, str: str, *, opener: str = None) -> None:
        self._str = str
        self._opener = "" if opener is None else opener

    def format(self, *args, **kwds) -> str:
        return self._str.format(*args, **kwds)


human: format = stringformat(
    "{function_name}:\n"
    "  process time:\tavg {avg_ptime} secs\tbest {best_ptime} secs\tworst {worst_ptime} secs\n"
    "  real time:\tavg {avg_rtime} secs\tbest {best_rtime} secs\tworst {worst_rtime} secs"
)
"""Human readable output format."""

csv: format = stringformat(
    "{function_name},"
    "{avg_ptime},{best_ptime},{worst_ptime},"
    "{avg_rtime},{best_rtime},{worst_rtime}",
    opener=(
        "function name,"
        "average ptime,best ptime,worst ptime,"
        "average rtime,best rtime,worst rtime\n"
    ),
)
"""Comma-separated values (CSV) format."""
