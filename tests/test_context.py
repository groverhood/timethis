from unittest import TestCase

from timethis import TimingContext, timethis
from io import StringIO
import tempfile


class TestContext(TestCase):
    def setUp(self) -> None:
        self.output = StringIO()
        self.context = TimingContext(file=self.output)

        @self.context.timethis
        def func():
            l = []
            for _ in range(1000):
                l.append(0)

        self.func = func

        @self.context.timethis
        def second_func():
            l = []
            for _ in range(1000):
                l.append(0)

        self.second_func = second_func

        @timethis
        def context_func():
            l = []
            for _ in range(1000):
                l.append(0)

        self.context_func = context_func

    def testTenRuns(self):
        for _ in range(10):
            self.func()

        d = dict(self.context)

        self.assertEqual(len(d), 1)
        self.assertIn(self.func.__name__, d)

        rec = d[self.func.__name__]
        self.assertEqual(rec.total_runs, 10)

    def testHundredRuns(self):
        for _ in range(100):
            self.func()

        d = dict(self.context)

        self.assertEqual(len(d), 1)
        self.assertIn(self.func.__name__, d)

        rec = d[self.func.__name__]
        self.assertEqual(rec.total_runs, 100)

    def testTwoFunctions(self):
        for _ in range(10):
            self.func()

        for _ in range(10):
            self.second_func()

        d = dict(self.context)

        self.assertEqual(len(d), 2)
        self.assertIn(self.func.__name__, d)

        rec = d[self.func.__name__]
        self.assertEqual(rec.total_runs, 10)

        rec = d[self.second_func.__name__]
        self.assertEqual(rec.total_runs, 10)

    def testContextManager(self):
        with self.context:
            self.context_func()

        self.output.seek(0)
        output = self.output.read()

        float_pattern = r"[-+]?(\d+([.,]\d*)?|[.,]\d+)([eE][-+]?\d+)?"
        self.assertRegex(
            output,
            (
                r"[a-zA-Z_][a-zA-Z0-9_]:"
                "\n"
                r"  process time:"
                "\t"
                r"avg " + float_pattern + r" secs"
                "\t"
                r"best " + float_pattern + r" secs"
                "\t"
                r"worst " + float_pattern + r" secs\n"
                r"  real time:"
                "\t"
                r"avg " + float_pattern + r" secs"
                "\t"
                r"best " + float_pattern + r" secs"
                "\t"
                r"worst " + float_pattern + r" secs\n"
            ),
        )

    def testPath(self):
        tmpfile = tempfile.NamedTemporaryFile("w+")

        self.context = TimingContext(file=tmpfile.name)
        with self.context:
            self.context_func()
        with open(tmpfile.name) as file:
            output = file.read()
            float_pattern = r"[-+]?(\d+([.,]\d*)?|[.,]\d+)([eE][-+]?\d+)?"
            self.assertRegex(
                output,
                (
                    r"[a-zA-Z_][a-zA-Z0-9_]:"
                    "\n"
                    r"  process time:"
                    "\t"
                    r"avg " + float_pattern + r" secs"
                    "\t"
                    r"best " + float_pattern + r" secs"
                    "\t"
                    r"worst " + float_pattern + r" secs\n"
                    r"  real time:"
                    "\t"
                    r"avg " + float_pattern + r" secs"
                    "\t"
                    r"best " + float_pattern + r" secs"
                    "\t"
                    r"worst " + float_pattern + r" secs\n"
                ),
            )
