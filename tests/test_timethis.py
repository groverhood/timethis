from unittest import TestCase

from io import StringIO
from timethis import timethis, TimingContext


class TestTimethis(TestCase):
    def setUp(self) -> None:
        self.output = StringIO()
        self.context = TimingContext(file=self.output)

    def testTimethis(self) -> None:
        @timethis
        def func():
            return sum([1] * 1000)

        with self.context:
            self.assertIsNotNone(TimingContext.current())
            self.assertIs(TimingContext.current(), self.context)

            for _ in range(10):
                func()

        self.output.seek(0)
        output = self.output.read()

        float_pattern = r"[-+]?(\d+([.,]\d*)?|[.,]\d+)([eE][-+]?\d+)?"
        self.assertRegex(
            output,
            (
                r"[a-zA-Z_][a-zA-Z0-9_]:"
                "\n"
                r"  process time:"
                "\t"
                r"avg " + float_pattern + r" secs"
                "\t"
                r"best " + float_pattern + r" secs"
                "\t"
                r"worst " + float_pattern + r" secs\n"
                r"  real time:"
                "\t"
                r"avg " + float_pattern + r" secs"
                "\t"
                r"best " + float_pattern + r" secs"
                "\t"
                r"worst " + float_pattern + r" secs\n"
            ),
        )
