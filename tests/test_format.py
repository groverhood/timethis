import timethis.format

from unittest import TestCase
from timethis.format import stringformat


class TestFormat(TestCase):
    def testMatchesStringFormat(self):
        a, b, c = 12, 3.4, "abc"
        format_str = "{a} {b} {c}"
        self.assertEqual(
            format_str.format(a=a, b=b, c=c),
            stringformat(format_str).format(a=a, b=b, c=c),
        )

    def testHumanFormat(self):
        format = timethis.format.human
        expected_output = (
            "foo:\n"
            "  process time:\tavg 0 secs\tbest 0 secs\tworst 0 secs\n"
            "  real time:\tavg 0 secs\tbest 0 secs\tworst 0 secs"
        )

        self.assertEqual(
            format.format(
                function_name="foo",
                avg_ptime=0,
                best_ptime=0,
                worst_ptime=0,
                avg_rtime=0,
                best_rtime=0,
                worst_rtime=0,
            ),
            expected_output,
        )

    def testCSVFormat(self):
        format = timethis.format.csv
        expected_output = "foo,0,0,0,0,0,0"
        self.assertEqual(
            format.format(
                function_name="foo",
                avg_ptime=0,
                best_ptime=0,
                worst_ptime=0,
                avg_rtime=0,
                best_rtime=0,
                worst_rtime=0,
            ),
            expected_output,
        )
