name = "timethis"
version = "0.0.1"
description = "Small, flexible, and extensible timing library"
python_requires = ">=3.9"
author = "Duncan Huntsinger"
author_email = "aloginna@cs.utexas.edu"
maintainer = "Duncan Huntsinger"
maintainer_email = "aloginna@cs.utexas.edu"
classifiers = [
    "Intended Audience :: Developers",
    "License :: OSI Approved :: The Unlicense (Unlicense)",     
    "Operating System :: OS Independent",        
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.9",
    "Typing :: Typed"
]