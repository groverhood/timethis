Introduction
============

**timethis** is a Python module designed as an elegant alternative to the 
builtin timeit module. Its functionality primarily consists of the 
:class:`TimingContext <timethis.context.TimingContext>` class and the function
decorator :func:`timethis <timethis.timethis>`. These features allow for the
simple insertion of timing statements by adding decorating statements above
the functions which need to be timed. **timethis** tracks CPU time as well as
real time when measuring function runtimes, and offers multiple formats for
representing the collected data.