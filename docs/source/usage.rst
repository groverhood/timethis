Usage
=====

There are two main ways to make use of **timethis**. One way is to use a
:class:`TimingContext <timethis.context.TimingContext>` as a context manager
with the :func:`timethis <timethis.timethis>` decorator:

.. code-block:: python
    @timethis
    def func(iterable):
        ...

    data = ...

    with TimingContext():
        for _ in range(10):
            # Time data is collected each time func is called
            func()

    # When the with block closes a human readable table will be printed
    # describing the results.

This method is simple, but doesn't allow for much flexibility when trying to
track timing data across different scopes and modules, and makes editing more
complex. The other approach is to use the 
:meth:`timethis <timethis.context.TimingContext.timethis>` method as a decorator:

.. code-block:: python
    context = TimingContext()

    @context.timethis
    def func(iterable):
        ...

    for _ in range(10):
        func()

This method allows for timing data to be tracked over regular control flows with
minimal editing, but output will only be seen when the context's scope ends.