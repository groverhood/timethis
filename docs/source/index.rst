.. timethis documentation master file, created by
   sphinx-quickstart on Tue Jul 27 21:32:12 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to timethis's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: About

   intro
   usage

.. toctree::
   :maxdepth: 2
   :caption: API Documentation

   timethis
   timethis.context
   timethis.format

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
