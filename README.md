# timethis

This module provides an alternative to the standard `timeit` package for timing functions. The core tenet of this module is simplicity. There is minimal work needed to set up timing for various functions:

```py
from timethis import TimingContext

context = TimingContext()

# Time this function using context
@context.timethis
def func(iterable):
    ...

data = ...
# Run func() 1000 times to get the average runtime over 1000 calls
for _ in range(1000):
    func(data)
```

When used as a decorator, a `TimingContext` object will output timing data just before it is garbage collected. It can also be used as a context manager:

```py
from timethis import TimingContext, timethis

@timethis
def func(iterable):
    ...

data = ...
with TimingContext():
    for _ in range(1000):
        func(data)
```

By default, a `TimingContext` object will print its output to `stdout`, but this can be changed by specifying the `file` parameter in its constructor to a path string or IO object. Currently the following output formats are supported:
* Human readable
* CSV

`TimingContext` objects also allow for iteration over the key-value pairs in their timing database, as well as indexing using either functions or function names.

Full documentation can be found at https://groverhood.gitlab.io/timethis