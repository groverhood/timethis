
all:

.FORCE:

docs: .FORCE
	$(MAKE) -C docs clean html

install: .FORCE
	python setup.py install